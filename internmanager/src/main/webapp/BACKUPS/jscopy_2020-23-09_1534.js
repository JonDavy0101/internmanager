/* *********************************
 * Global Variables
 ***********************************/
var internsArray = [[400000, "jon", "cha", "987", "1992-08-08", "2020-08-07"]];
var addInternBtn = orly.qid("addInternBtn");
var internMaxId = 399999;
var table;

// DOM ready
orly.ready.then(function() {	
	// Jon - target edit & delete btn
	table = document.getElementById("bodyContentTile");
	table.addEventListener("click", function(e){
		var id = e.target.id;
		//var id = e.target;
		console.log("Id of element clicked is: " + id.substring(id.indexOf("4")));
		console.log(id.substring(0,1));
		if (id.substring(0,2) == "e_") {
			global.editIntern(+id.substring(2));
		} else if (id.substring(0,2) == "d_") {
			global.deleteIntern();
		} else if (id == "addInternBtn") {
			global.addIntern();
		} else if (id == "submitBtn") {
			global.submitIntern()
		}
	});

	// Your table and your Add Intern divs are both showing.
	// You need to update the function "showAndHideElements"!!!
	/*global.showAndHideElements("internTableDiv", "addEditInternDiv");*/
	
	global.displayInterns(internsArray);
	
	// Welcome message
	orly.qid("alerts").createAlert({type:"success", duration:"2000", msg:"Welcome to Intern Manager v0.001"});
	
	// Jon - Initially hide form
	document.getElementById("addEditInternDiv").style.display = "none";
});	

/* *********************************
 * Global Namespace For Functions
 ***********************************/
var global = {		
	
	hello : function() {
		console.log("hello world!");
	},
		
		
	/**
	  * Display Interns 
	  */
	displayInterns : function() {
		console.log("Display Interns Called");
		this.buildAndDisplayInternTable();
		
	}, // end of displayInterns
	
	
	/**
	  * Build and display Intern Table 
	  */
	buildAndDisplayInternTable : function() {
		// TODO: build intern table
		// Build the header
		
		// If the internsArray is empty: 
		// 	   show an empty row that spans all columns
		//     the row should read "No Data Found"
		
		// If the internsArray is not empty:
		// Sort the interns (by first and/or last name)
	    // Delete the existing table
	    // Build a new table
	    
		console.log("buildAndDisplayInternTable called.");
		this.buildInternTable();
		
	}, // end of buildAndDisplayInternTable
	
	
	/**
	  * Build Intern Table 
	  */
	buildInternTable : function() {
		let tbody = document.createElement('tbody');
		document.getElementById("table").appendChild(tbody);
		document.getElementById("table").lastChild.id = "tbody";
		
		// Jon - sort internsArray by name
		internsArray.sort(function(a, b) {
			var nameA = a[1].toUpperCase(); // ignore upper and lowercase
			var nameB = b[1].toUpperCase(); // ignore upper and lowercase
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}
			// names must be equal
			return 0;
		});

		if (internsArray.length > 0) {
			// Loop through interns
			internsArray.forEach((intern, index) => {
				console.log(intern);

				// Find max intern id;
				if (internsArray[index][0] >= internMaxId) {
					internMaxId = internsArray[index][0];
				}

				
				let tableRow = document.createElement('tr');

				let editDelCol = document.createElement('td');
				let idCol = document.createElement('td');
				let firstNameCol = document.createElement('td');
				let lastNameCol = document.createElement('td');
				let ageCol = document.createElement('td');
				let startDateCol = document.createElement('td');
				let gradDateCol = document.createElement('td');

				let idTxt = document.createTextNode(internsArray[index][0]);
				let firstNameTxt = document.createTextNode(internsArray[index][1]);
				let lastNameTxt = document.createTextNode(internsArray[index][2]);
				let ageTxt = document.createTextNode(internsArray[index][3]);
				let startDateTxt = document.createTextNode(internsArray[index][4]);
				let gradDateTxt = document.createTextNode(internsArray[index][5]);

				editDelCol.innerHTML = `<orly-icon id='e_${internsArray[index][0]}' class='pr-2' color='wheat' name='edit'></orly-icon><orly-icon id='d_${internsArray[index][0]}' color='red' name='x'></orly-icon>`;
				idCol.appendChild(idTxt);
				firstNameCol.appendChild(firstNameTxt);
				lastNameCol.appendChild(lastNameTxt);
				ageCol.appendChild(ageTxt);
				startDateCol.appendChild(startDateTxt);
				gradDateCol.appendChild(gradDateTxt);

				tableRow.appendChild(editDelCol);
				tableRow.appendChild(idCol);
				tableRow.appendChild(firstNameCol);
				tableRow.appendChild(lastNameCol);
				tableRow.appendChild(ageCol);
				tableRow.appendChild(startDateCol);
				tableRow.appendChild(gradDateCol);

				document.getElementById("tbody").appendChild(tableRow);
			});
		} else {
			let tableRow = document.createElement('tr');
			let noDataCol = document.createElement('td');

			let noDataTxt = document.createTextNode("There are no interns to display");

			noDataCol.appendChild(noDataTxt);
			tableRow.appendChild(noDataCol);
			document.getElementById("tbody").appendChild(tableRow);
		}

		
	}, // end of buildInternTable
	
	
	/**
	  * Add Table Row 
	  */
	addTableRow : function(intern) {
		// Add table row for a given intern 
		
		
	}, // end of addTableRow
	
	
	/**
	  * Add Intern - Called when submitting the "Add New Intern" Form
	  */
	addIntern: function(/*firstName, lastName, age, startDate, graduationDate*/) {
		// Jon - hide intern table and show form
		document.getElementById("addEditInternDiv").style.display = "block";
		document.getElementById("internTableDiv").style.display = "none";
			
		
		// Generate Intern Id
		let internId = this.getInternId();
		
		// Build Intern
		let intern = {};
		
		// Add intern to internsArray
		
		
		// Return Intern?
		return intern;
		
	}, // end of addIntern


	/**
	  * Submit Intern Data
	  */
	submitIntern: function() {
		var idNum = 0;
		var idIndex = internsArray.length;

		var subId = document.getElementById("internId");
		var subFirstName = document.getElementById("addFirstName");
		var subLastName = document.getElementById("addLastName");
		var subAge = document.getElementById("addAge");
		var subStartDate = document.getElementById("addStartDate");
		var subGradDate = document.getElementById("addGradDate");

		if (subId.innerHTML == "") {
			idNum = ++internMaxId;
		} else {
			idNum = parseInt(subId.innerHTML);
		}

		internsArray.forEach((intern, index) => {
			if (intern == idNum) {
				idIndex = index;
			}
		});

		console.log(`idIndex: ${idIndex}`);

		// Jon - add data to internsArray
		internsArray[idIndex] = [idNum, subFirstName.value, subLastName.value, subAge.value, subStartDate.value, subGradDate.value];

		// Jon - delete tbody. read data from internsArray.
		// Display table and hide form.
		document.getElementById("tbody").remove();
		global.buildInternTable();
		document.getElementById("internTableDiv").style.display = "block";
		document.getElementById("addEditInternDiv").style.display = "none";
		document.getElementById("internId").innerHTML = "";
	}, // end of submitIntern
	
	
	/**
	  * Get Intern Id
	  */
	getInternId: function() {
		let id = null;
		return id;
		
	}, // end of getInternId
	
	
	/**
	  * Edit Intern - Called after editing an intern 
	  */
	editIntern: function(id) {
		var index = global.findInternById(id);
		// Jon - hide intern table and show form
		document.getElementById("addEditInternDiv").style.display = "block";
		document.getElementById("internTableDiv").style.display = "none";
		
		// Populate form with intern data
		document.getElementById("internId").innerHTML = internsArray[index][0];
		document.getElementById("addFirstName").value = internsArray[index][1];
		document.getElementById("addLastName").value = internsArray[index][2];
		document.getElementById("addAge").value = internsArray[index][3];
		document.getElementById("addStartDate").value = internsArray[index][4];
		document.getElementById("addGradDate").value = internsArray[index][5];
		
	}, // end of editIntern
	
	
	/**
	  * Delete Intern 
	  */
	deleteIntern: function() {


		document.getElementById("addEditInternDiv").style.display = "block";
		document.getElementById("internTableDiv").style.display = "none";

		// Delete the intern from the internsArray
		// Update the table
		
	}, // end of deleteIntern

	
	/**
	  * Display Success Message 
	  */
	displaySuccess: function(message) {
	
	}, // end of displaySuccess
	
	
	/**
	  * Display Error Message 
	  */
	displayError: function(message) {
		
	}, // end of displayError
	
	
	/**
	  * Find Intern By Key
	  */
	findInternById: function(id) {
		var ans = 0;
		
		// Jon - loop through internsArray and return 
		// the matching index
		internsArray.forEach((intern, index) => {
			if (intern[0] == id) {
				ans = index;
			}
		});
		
		return ans;
		
	} // end of findInternById 
	
}; // end of global