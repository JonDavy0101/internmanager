/* *********************************
 * Global Variables
 ***********************************/
var internsArray = [];
var addInternBtn = orly.qid("addInternBtn");
var internMaxId = 399999;
var table;
var form;

// DOM ready
orly.ready.then(function() {	
	form = orly.qid("form");
	// Jon - target edit & delete btn
	table = orly.qid("bodyContentTile");
	table.addEventListener("click", function(e){
		var id = e.target.id;
		//var id = e.target;
		console.log("Id of element clicked is: " + id.substring(id.indexOf("4")));
		console.log(id.substring(0,1));
		if (id.substring(0,2) == "e_") {
			global.editIntern(+id.substring(2));
		} else if (id.substring(0,2) == "d_") {
			global.deleteIntern(+id.substring(2));
		} else if (id == "addInternBtn") {
			global.addIntern();
		} else if (id == "submitBtn") {
			global.validateForm()
		}
	});
	
	//Jon - target start date to fill in grad date
	orly.qid("addStartDate").addEventListener("change", function() {
		let startDate = orly.qid("addStartDate");
		let gradDate = global.calcGradDate(startDate.value);
		let convertGradDate = global.convertDateForInput(gradDate);
		orly.qid("addGradDate").value = convertGradDate;
		//alert(gradDate);
	})

	// Your table and your Add Intern divs are both showing.
	// You need to update the function "showAndHideElements"!!!
	/*global.showAndHideElements("internTableDiv", "addEditInternDiv");*/
	
	global.buildInternTable();
	
	// Welcome message
	orly.qid("alerts").createAlert({type:"success", duration:"2000", msg:"Welcome to Intern Manager v0.001"});
	
	// Jon - Initially hide form
	orly.qid("addEditInternDiv").style.display = "none";
});	

/* *********************************
 * Global Namespace For Functions
 ***********************************/
var global = {		
	
	hello : function() {
		console.log("hello world!");
	},
	
	
	/**
	  * Build Intern Table 
	  */
	buildInternTable : function() {
		let tbody = document.createElement('tbody');
		orly.qid("table").appendChild(tbody);
		orly.qid("table").lastChild.id = "tbody";
		
		// Jon - sort internsArray by name
		internsArray.sort(function(a, b) {
			var nameA = a[1].toUpperCase(); // ignore upper and lowercase
			var nameB = b[1].toUpperCase(); // ignore upper and lowercase
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}
			// names must be equal
			return 0;
		});

		if (internsArray.length > 0) {
			// Loop through interns
			internsArray.forEach((intern, index) => {
				console.log(intern);

				// Find max intern id;
				if (internsArray[index][0] >= internMaxId) {
					internMaxId = internsArray[index][0];
				}

				
				let tableRow = document.createElement('tr');

				let editDelCol = document.createElement('td');
				let idCol = document.createElement('td');
				let firstNameCol = document.createElement('td');
				let lastNameCol = document.createElement('td');
				let ageCol = document.createElement('td');
				let startDateCol = document.createElement('td');
				let gradDateCol = document.createElement('td');

				let idTxt = document.createTextNode(internsArray[index][0]);
				let firstNameTxt = document.createTextNode(internsArray[index][1]);
				let lastNameTxt = document.createTextNode(internsArray[index][2]);
				let ageTxt = document.createTextNode(internsArray[index][3]);
				let startDateTxt = document.createTextNode(internsArray[index][4]);
				let gradDateTxt = document.createTextNode(internsArray[index][5]);

				editDelCol.innerHTML = `<orly-icon id='e_${internsArray[index][0]}' class='pr-2' color='wheat' name='edit'></orly-icon><orly-icon id='d_${internsArray[index][0]}' color='red' name='x'></orly-icon>`;
				idCol.appendChild(idTxt);
				firstNameCol.appendChild(firstNameTxt);
				lastNameCol.appendChild(lastNameTxt);
				ageCol.appendChild(ageTxt);
				startDateCol.appendChild(startDateTxt);
				gradDateCol.appendChild(gradDateTxt);

				tableRow.appendChild(editDelCol);
				tableRow.appendChild(idCol);
				tableRow.appendChild(firstNameCol);
				tableRow.appendChild(lastNameCol);
				tableRow.appendChild(ageCol);
				tableRow.appendChild(startDateCol);
				tableRow.appendChild(gradDateCol);

				orly.qid("tbody").appendChild(tableRow);
			});
		} else {
			let tableRow = document.createElement('tr');
			let noDataCol = document.createElement('td');

			let noDataTxt = document.createTextNode("There are no interns to display");

			noDataCol.appendChild(noDataTxt);
			tableRow.appendChild(noDataCol);
			orly.qid("tbody").appendChild(tableRow);
		}

		
	}, // end of buildInternTable
	
	
	/**
	  * Add Intern - Called when submitting the "Add New Intern" Form
	  */
	addIntern: function(/*firstName, lastName, age, startDate, graduationDate*/) {
		// Jon - hide intern table and show form
		orly.qid("addEditInternDiv").style.display = "block";
		orly.qid("internTableDiv").style.display = "none";
	}, // end of addIntern
	
	/**
	 * Validate Form
	 */
	validateForm: function() {
		if (form.reportValidity() == true) {
			global.submitIntern();
		} else {
			alert("Please fill out all fields")
		}
	}, // end of validateForm


	/**
	  * Submit Intern Data
	  */
	submitIntern: function() {
		var idNum = 0;
		var idIndex = internsArray.length;

		var subId = orly.qid("internId");
		var subFirstName = orly.qid("addFirstName");
		var subLastName = orly.qid("addLastName");
		var subAge = orly.qid("addAge");
		var subStartDate = orly.qid("addStartDate");
		var subGradDate = orly.qid("addGradDate");

		if (subId.innerHTML == "") {
			idNum = ++internMaxId;
		} else {
			idNum = parseInt(subId.innerHTML);
		}

		internsArray.forEach((intern, index) => {
			if (intern[0] == idNum) {
				idIndex = index;
			}
		});

		console.log(`idIndex: ${idIndex}`);

		// Jon - add data to internsArray
		internsArray[idIndex] = [idNum, subFirstName.value, subLastName.value, subAge.value, global.convertDate(subStartDate.value), global.convertDate(subGradDate.value)];

		// Jon - delete tbody. read data from internsArray.
		// Display table and hide form.
		orly.qid("tbody").remove();
		global.buildInternTable();
		orly.qid("internTableDiv").style.display = "block";
		orly.qid("addEditInternDiv").style.display = "none";
		orly.qid("internId").innerHTML = "";
		orly.qid("form").reset();
	}, // end of submitIntern
	
	
	/**
	  * Edit Intern - Called after editing an intern 
	  */
	editIntern: function(id) {
		var index = global.findInternById(id);
		// Jon - hide intern table and show form
		orly.qid("addEditInternDiv").style.display = "block";
		orly.qid("internTableDiv").style.display = "none";
		
		// Populate form with intern data
		orly.qid("internId").innerHTML = internsArray[index][0];
		orly.qid("addFirstName").value = internsArray[index][1];
		orly.qid("addLastName").value = internsArray[index][2];
		orly.qid("addAge").value = internsArray[index][3];
		orly.qid("addStartDate").value = global.convertDateForInput(internsArray[index][4]);
		orly.qid("addGradDate").value = global.convertDateForInput(internsArray[index][5]);
		
	}, // end of editIntern
	
	
	/**
	  * Delete Intern 
	  */
	deleteIntern: function(id) {
		index = global.findInternById(id);
		name = internsArray[index][1] + " " + internsArray[index][2];
		console.log(`index: ${index}`);
		console.log(`name: ${name}`);

		// Jon - delete the intern from the internsArray
		if (confirm(`Are you sure you want to delete ${name} (ID ${id})?`)) {
		    internsArray.splice(index,1);
		}

		// Jon - update the table
		orly.qid("tbody").remove();
		global.buildInternTable();
		
	}, // end of deleteIntern
	
	
	/**
	  * Find Intern By Key
	  */
	findInternById: function(id) {
		var ans = 0;
		
		// Jon - loop through internsArray and return 
		// the matching index
		internsArray.forEach((intern, index) => {
			if (intern[0] == id) {
				ans = index;
			}
		});
		
		return ans;
		
	}, // end of findInternById 
	
	
	/**
	 * Calculate graduation date
	 */
	calcGradDate: function(currentDate) {
	// findInternById: function(id) {
		var date = new Date(currentDate);
		var gradDate = new Date(date.getTime() + 7257600000);
		
		// console.log(gradDate);
		// return global.convertDate(gradDate);
		return global.convertDate(gradDate);
	}, // end of calcGradDate
	
	
	/**
	 * Convert date to proper format
	 */
	convertDate: function(date) {
		var day, month, year;
		var newDate = new Date(date);
		day = newDate.getDate();
		year = newDate.getFullYear();
		
		// Jon - find month abbreviation
		switch (newDate.getMonth() + 1) {
			case 1:
				month = "Jan";
				break;
			case 2:
				month = "Feb";
				break;
			case 3:
				month = "Mar";
				break;
			case 4:
				month = "Apr";
				break;
			case 5:
				month = "May";
				break;
			case 6:
				month = "Jun";
				break;
			case 7:
				month = "Jul";
				break;
			case 8:
				month = "Aug";
				break;
			case 9:
				month = "Sep";
				break;
			case 10:
				month = "Oct";
				break;
			case 11:
				month = "Nov";
				break;
			case 12:
				month = "Dec";
				break;
			default:
				console.log("invalid month entered");
				break;
		}
		
		// Jon - return proper date format
		return `${month} ${day}, ${year}`;
		
	}, // end of convertDate
	
	convertDateForInput(date) {
		var convertedDate = new Date(date);
		var firstIndex = convertedDate.toLocaleDateString().indexOf("/");
		var lastIndex = convertedDate.toLocaleDateString().lastIndexOf("/");
		var month = convertedDate.toLocaleDateString().slice(0,firstIndex);
		var day = convertedDate.toLocaleDateString().slice((firstIndex + 1),lastIndex);
		var year = convertedDate.toLocaleDateString().slice(lastIndex + 1);
		
		// Jon - set correct month format
		switch (month) {
			case "1":
				month = "01";
				break;
			case "2":
				month = "02";
				break;
			case "3":
				month = "03";
				break;
			case "4":
				month = "04";
				break;
			case "5":
				month = "05";
				break;
			case "6":
				month = "06";
				break;
			case "7":
				month = "07";
				break;
			case "8":
				month = "08";
				break;
			case "9":
				month = "09";
			default:
				break;
		}
		
		// Jon - set correct day format
		switch (day) {
			case "1":
				day = "01";
				break;
			case "2":
				day = "02";
				break;
			case "3":
				day = "03";
				break;
			case "4":
				day = "04";
				break;
			case "5":
				day = "05";
				break;
			case "6":
				day = "06";
				break;
			case "7":
				day = "07";
				break;
			case "8":
				day = "08";
				break;
			case "9":
				day = "09";
				break;
			default:
				break;
		}
		
		return `${year}-${month}-${day}`;
	}
	
}; // end of global